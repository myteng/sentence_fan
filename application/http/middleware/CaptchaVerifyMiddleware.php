<?php

namespace app\http\middleware;

use app\common\library\CaptchaAuth;
use traits\controller\Jump;

class CaptchaVerifyMiddleware
{
    use Jump;

    public function handle($request, \Closure $next)
    {
        if (! (new CaptchaAuth)->verify($request->post('captcha'))) {
            return $this->error('验证码错误');
        }
        return $next($request);
    }

}
