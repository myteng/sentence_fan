<?php

namespace app\common\model;

use think\Model;

class Category extends Model
{

    protected $table = 'categories';

    protected $field = [
        'name',
    ];

    public function tags()
    {
        return $this->hasMany(Tag::class, 'category_id');
    }

}
