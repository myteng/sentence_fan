<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/12
 * Time: 20:08
 */

namespace app\common\library;

use app\common\model\User;
use think\facade\Session;

class Auth
{

    protected static $instance;

    const SESSION_KEY = 'user_id';

    protected static $user;

    private function __construct()
    {
    }

    public static function getInstance()
    {
        if (! self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function check()
    {
        return Session::has(self::SESSION_KEY) && Session::get(self::SESSION_KEY);
    }

    public function id()
    {
        return Session::get(self::SESSION_KEY);
    }

    public function user()
    {
        if (! self::$user) {
            self::$user = User::find(Session::get(self::SESSION_KEY));
        }
        return self::$user;
    }

}