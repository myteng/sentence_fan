<?php
namespace app\common\library;

use think\facade\Session;

class CaptchaAuth
{

    const STORE_KEY = 'captcha';

    public function verify($captcha)
    {
        $storeCaptcha = Session::get(self::STORE_KEY);
        if (! $storeCaptcha) {
            return false;
        }
        return $storeCaptcha == strtolower($captcha);
    }

    public function store($captcha)
    {
        Session::set(self::STORE_KEY, strtolower($captcha));
    }

}