<?php

namespace app\common\validate;

use think\Validate;

class SentenceCreateValidator extends Validate
{
	protected $rule = [
	    'sentence' => 'require|min:10|max:255',
    ];

    protected $message = [
        'sentence.require' => '请填写句子',
        'sentence.min' => '句子不能少于10个字',
        'sentence.max' => '句子不能超过255个字',
    ];
}
