<?php

namespace app\common\validate;

use think\Validate;

class MemberPasswordChangeValidate extends Validate
{
	protected $rule = [
	    'old_password|原密码' => 'require',
        'new_password|新密码' => 'require|min:6|max:16|confirm:new_password_confirm',
    ];

    protected $message = [];
}
