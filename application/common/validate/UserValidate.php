<?php

namespace app\common\validate;

use think\Validate;

class UserValidate extends Validate
{

	protected $rule = [
	    'username|用户名' => 'require|min:2|max:16|unique:users,username',
        'password|密码' => 'require|min:6|max:16|confirm:password_confirm',
    ];

    protected $message = [];
}
