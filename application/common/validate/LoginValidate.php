<?php

namespace app\common\validate;

use think\Validate;

class LoginValidate extends Validate
{

	protected $rule = [
	    'username|用户名' => 'require|min:2|max:16',
        'password|密码' => 'require|min:6|max:16',
    ];

    protected $message = [];
}
