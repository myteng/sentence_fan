<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

function url1($url = '', $vars = '', $suffix = true, $domain = false)
{
    $url = url($url, $vars, $suffix, $domain);
    return preg_replace_callback('/\/\/(.*?)\//', function($value) {
        $appHost = config('app.app_host');
        // http | https
        $appHost = preg_match('/http/', $appHost) ? str_replace(['http://', 'https://'], '', $appHost) : $appHost;
        $appHost = str_replace('/', '', $appHost);
        return '//'.$appHost.'/';
    }, $url);
}