<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/8/12
 * Time: 19:58
 */

namespace tests;


use app\common\model\User;
use think\facade\Session;

class LoginTest extends TestCase
{

    static $user;
    static $userData;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        self::$userData = [
            'username' => bin2hex(random_bytes(8)),
            'password' => '123456',
        ];

        self::$user = new User;
        self::$user->save(self::$userData);
    }

    public static function tearDownAfterClass()
    {
        parent::tearDownAfterClass();

        self::$user->delete();
    }

    public function test_login()
    {
        $this->visit('/login')
            ->submitForm('登陆', self::$userData)
            ->see('登陆成功');

        $this->assertTrue(Session::has('user_id'));
    }

}