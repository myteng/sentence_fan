<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateTagsTable extends Migrator
{

    public function up()
    {
        $nameColumn = (new Column)->setName('name')->setType('string')->setLimit(16)->setComment('标签名');
        $this->table('tags')->addColumn($nameColumn)->addTimestamps()->create();
    }

    public function down()
    {
        $this->dropTable('tags');
    }

}
