<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AddAvatarColumnForUsersTable extends Migrator
{
    public function up()
    {
        $avatarColumn = (new  Column)->setName('avatar')
            ->setType('string')
            ->setLimit(255)
            ->setComment('头像')
            ->setDefault('')
            ->setAfter('username');
        $table = $this->table('users');
        $table->addColumn($avatarColumn)->save();
    }

    public function down()
    {
        $this->table('users')->removeColumn('avatar');
    }
}
