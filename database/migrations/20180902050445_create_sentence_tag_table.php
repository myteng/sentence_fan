<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateSentenceTagTable extends Migrator
{

    public function up()
    {
        $table = $this->table('sentence_tag');

        $sentenceId = (new Column)->setName('sentence_id')->setType('integer')->setComment('句子ID');
        $tagId = (new Column)->setName('tag_id')->setType('integer')->setComment('标签ID');

        $table->addColumn($sentenceId)->addColumn($tagId)->create();
    }

    public function down()
    {
        $this->dropTable('sentence_tag');
    }

}
