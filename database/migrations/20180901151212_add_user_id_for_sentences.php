<?php

use think\migration\Migrator;
use think\migration\db\Column;

class AddUserIdForSentences extends Migrator
{

    public function up()
    {
        $userIdColumn = (new Column)->setName('user_id')->setAfter('sentence')->setComment('用户ID')->setDefault(0);
        $this->table('sentences')->addColumn($userIdColumn)->save();
    }

    public function down()
    {
        $this->table('sentences')->removeColumn('user_id');
    }

}
