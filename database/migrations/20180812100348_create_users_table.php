<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateUsersTable extends Migrator
{
    public function up()
    {
        $table = $this->table('users');

        $usernameColumn = (new Column())->setName('username')->setType('string')->setLimit(16)->setUnique();
        $passwordColumn = (new Column())->setName('password')->setType('string')->setLimit(128);

        $table->addColumn($usernameColumn)
            ->addColumn($passwordColumn)
            ->addTimestamps();

        $table->create();
    }

    public function down()
    {
        $this->hasTable('users') && $this->dropTable('users');
    }
}
