<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateSentencesTable extends Migrator
{

    public function up()
    {
        $table = $this->table('sentences');
        $table->addColumn('sentence', 'string', ['limit' => 255])
            ->addTimestamps();
        $table->create();
    }

    public function down()
    {
        $this->hasTable('sentences') && $this->dropTable('sentences');
    }

}
