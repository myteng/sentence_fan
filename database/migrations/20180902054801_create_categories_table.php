<?php

use think\migration\Migrator;
use think\migration\db\Column;

class CreateCategoriesTable extends Migrator
{
    public function up()
    {
        $table = $this->table('categories');

        $name = (new Column)->setName('name')->setType('string')->setLimit(32)->setComment('分类名');

        $table->addColumn($name)->addTimestamps()->create();
    }

    public function down()
    {
        $this->dropTable('categories');
    }
}
