<?php

use think\migration\Seeder;

class TagSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'name' => '国家',
                'children' => [
                    '美国', '英国', '法国', '德国', '日本',
                ],
            ],
            [
                'name' => '类型',
                'children' => [
                    '爱情', '读书', '励志', '时间', '友情', '诚信', '爱国',
                ],
            ],
        ];

        foreach ($data as $datum) {
            $category = \app\common\model\Category::create(['name' => $datum['name']]);
            $category->tags()->saveAll(array_map(function ($value) {
                    return ['name' => $value];
            }, $datum['children']));
        }
    }
}