<?php

use think\migration\Seeder;

class SentenceSeeder extends Seeder
{
    public function run()
    {
        $num = 100;
        $rows = [];
        while ($num > 0) {
            $rows[] = [
                'sentence' => '美国总统特朗普刚刚继续“落井下石”，他发布推特宣布，对土耳其钢铝产品加征关税翻倍，铝产品加征20%，钢铁产品加征50%，因为“土耳其里拉因强势美元迅速贬值，抵消了关税效果”。',
            ];
            $num--;
        }

//        \app\common\model\Sentence::insertAll($rows);
    }
}